# -*- mode: ruby -*-
# vi: set ft=ruby :
#
HOME = ENV['HOME']
ROOT_DIR = File.join(HOME, "dev/vagrant-bootstrap/")
SCRIPTS_DIR = File.join(ROOT_DIR, 'scripts')
MODULES_DIR = File.join(ROOT_DIR, 'modules')
MANIFESTS_DIR = File.join(ROOT_DIR, 'manifests')
DATA_DIR = File.join(ROOT_DIR, 'data')

USERNAME = "vagrant"
USERID = 1000

Vagrant.configure('2') do |config|

  config.vm.hostname = "xuan"
  config.vm.synced_folder HOME, HOME, create: true, type: "nfs"

  config.vm.provider :virtualbox do |provider, override|
    provider.name = File.basename(File.dirname(File.expand_path(__FILE__)))
    provider.customize ["modifyvm", :id, "--memory", "2048"]

    override.vm.box = "boxcutter/ubuntu1404"
    override.vm.network "private_network", ip: "192.168.4.22"
  end

  config.ssh.forward_agent = true
  config.ssh.username = USERNAME

  config.vm.provision "shell", path: File.join(SCRIPTS_DIR, 'su_ssh_auth_sock.sh')
  config.vm.provision "shell", path: File.join(SCRIPTS_DIR, 'apt_get_update.sh')
  config.vm.provision "shell", path: File.join(SCRIPTS_DIR, 'ubuntu_install_puppet.sh')
  config.vm.provision "shell", path: File.join(SCRIPTS_DIR, 'ubuntu_install_docker.sh')

  config.vm.provision :puppet do |puppet|
    puppet.facter = {
      "user" => USERNAME,
      "user_uid" => USERID,
      "user_gid" => USERID,
    }
    puppet.options = "--verbose --debug"
    puppet.manifests_path = MANIFESTS_DIR
    puppet.manifest_file  = "default.pp"
    puppet.module_path = [ MODULES_DIR ]
  end
end
