#global path
Exec { path => ["/usr/sbin/", "/usr/bin/", "/bin/", "/usr/local/bin"] }

case $operatingsystem {
    'centos': {
        service{ ['sendmail',
                  'iptables']:
            ensure => 'stopped'
        }
    }
}

node default {
  include apt_update
  include basic_dev
  include basic_python

  class {
    user:
  }
  ->
  file {
          "/home/${user}/dev":
    ensure => "directory",
    mode   => '0755',
    owner => "${user}",
    group => "${user}",
  }
  ->
  class {
    'ssh_config':
    user => "${user}"
  }
  ->
  class {
    'dotfiles':
    checkout_dir => "/home/${user}/dev",
    user => "${user}",
  }
  ->
  class {
    nfs_config:
  }
}
