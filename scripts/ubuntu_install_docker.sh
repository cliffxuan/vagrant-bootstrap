#!/usr/bin/env bash
echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" > /etc/apt/sources.list.d/docker.list
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
apt-get install -y apt-transport-https
apt-get update
if ! type docker &> /dev/null
then
    apt-get install -y --force-yes -q docker-engine
else
    apt-get upgrade -y docker-engine
fi

usermod -aG docker vagrant
