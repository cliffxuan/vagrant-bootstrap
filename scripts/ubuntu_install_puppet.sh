#!/usr/bin/env bash
if which puppet >/dev/null
then
    echo "puppet already installed"
else
    apt-get -y install puppet
fi
