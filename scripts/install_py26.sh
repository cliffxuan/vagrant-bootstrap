#keep up to date
apt-get update

#install python2.6
apt-get install python-software-properties
add-apt-repository ppa:fkrull/deadsnakes
apt-get update
read -p "which python2.6"
if [ ! -x $(which python2.6) ]
then
    apt-get install python2.6 python2.6-dev
else
    apt-get install python2.6-dev
fi

#install easy_install
curl -0 http://python-distribute.org/distribute_setup.py | python2.6
