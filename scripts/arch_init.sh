# set locale
echo 'en_GB.UTF-8 UTF-8' >> /etc/locale.gen
locale-gen
echo 'LANG=en_GB.UTF-8' > /etc/locale.conf
export LANG=en_GB.UTF-8

# choose a mirror
#sed -i 's/^#\(.*leaseweb.*\)/\1/' /etc/pacman.d/mirrorlist

# update pacman
pacman -Syu

#install puppet
#libyaml is necessary
pacman -S libyaml libxml2 libxslt git --noconfirm

# rvm with ruby 1.9.3 for puppet and chef
echo "gem: --no-rdoc --no-ri" > /root/.gemrc
\curl -#L https://get.rvm.io | bash -s stable --autolibs=3 --ruby=1.9.3 --gems=puppet,chef
echo "gem: --no-rdoc --no-ri" > /home/vagrant/.gemrc
chown vagrant:vagrant /home/vagrant/.gemrc
