#!/bin/bash
set -x
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
REPO_DIR=$DIR/..
cd $REPO_DIR/manifests
puppet apply \
--modulepath "/etc/puppet/modules:$REPO_DIR/modules" \
-e "Exec { path => ['/usr/sbin/', '/usr/bin/', '/bin', '/usr/local/bin/'] } include $1"
