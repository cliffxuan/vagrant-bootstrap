class basic_dev {

    include git
    include curl

    package {[
      "build-essential",
      "tmux",
      "vim",
      "zsh",
      "patch",
      "ack-grep",
      "silversearcher-ag"
      ]:
        ensure => installed
    }
    ->
    exec { "rename ack-grep":
        command => "dpkg-divert --local --divert /usr/bin/ack --rename --add /usr/bin/ack-grep",
        unless  => "test -x /usr/bin/ack"
    }
}
