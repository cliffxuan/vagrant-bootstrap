class apache2 {
    case $operatingsystem {
      'ubuntu': {
        require apache2::ubuntu
        }
      'centos':  {
        require apache2::centos
        }
    }
}
class apache2::ubuntu {
    package { [
                'apache2',
                'libapache2-mod-php5'
              ]:
      ensure => 'installed',
    }
}
class apache2::centos {
    package { [
                'httpd',
                'php-common'
              ]:
      ensure => 'installed',
    }
    service {'httpd':
        ensure => 'running'
    }
}
