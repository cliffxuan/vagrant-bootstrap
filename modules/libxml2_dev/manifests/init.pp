class libxml2_dev {
    case $operatingsystem {
      'ubuntu': {
        require libxml2_dev::ubuntu
        }
      'centos':  {
        require libxml2_dev::centos
        }
    }
}

class libxml2_dev::ubuntu {
    package {'libxml2-dev':
        ensure => installed
    }
}

class libxml2_dev::centos {
    package {'libxml2-devel':
        ensure => installed
    }
}
