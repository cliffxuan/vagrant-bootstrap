class user {
  group { $user:
    gid        => $user_gid,
    ensure     => 'present'
  }

  user { $user:
    name       => $user,
    uid        => $user_uid,
    gid        => $user_gid,
    home       => "/home/${user}",
    password   => '$6$/Zq2KK4fwbP$T1qccKsBTbsYi2q/3Ik9zaGQkZBYjOVTQ.EBsP6XX70.X8kUt0x5mNvLJ9S58pHFw.lXyzAZ5zgMenFBsh.zY1',
    shell      => '/bin/zsh',
    managehome => 'true',
    ensure     =>  'present'
  }

}
