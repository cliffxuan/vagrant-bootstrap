class ssh_config($user) {
    file { [ "/home/${user}/.ssh" ]:
        ensure => "directory",
        mode => '0755',
        owner => $user
    }
    file { [ "/home/${user}/.ssh/known_hosts" ]:
        source => "puppet:///modules/ssh_config/known_hosts",
        mode => 0600,
        owner => $user
    }
    file { [ "/home/${user}/.ssh/config" ]:
        content  => template('ssh_config/config.erb'),
        mode => 0600,
        owner => $user 
    }
}
