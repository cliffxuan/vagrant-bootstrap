class mysql {
    case $operatingsystem {
      'archlinux':  {
        require mysql::archlinux
        }
      'ubuntu': {
        require mysql::ubuntu
        }
      'centos':  {
        require mysql::centos
        }
    }

}

class mysql::ubuntu {

    package { [
        "mysql-server",
        "libmysqlclient-dev"
        ]:
        ensure => installed,
    }
}

class mysql::centos {

    package { [
        "mysql-server",
        "mysql-devel"
        ]:
        ensure => installed,
    }

    service { 'mysqld':
        ensure  => running,
        require => Package[ mysql-server ],
        enable  => true
    }
}

class mysql::archlinux {

    package { [
        'mysql',
        ]:
        ensure => installed,
    }
    /*service { 'mysqld':*/
        /*ensure  => running,*/
        /*require => Package[ 'mysql' ],*/
        /*enable  => true*/
    /*}*/
    exec { 'start':
        command => 'systemctl start mysqld.service',
        unless => 'systemctl status mysqld.service'
    }
    exec { 'enable service':
        command => 'systemctl enable mysqld.service',
        unless => 'systemctl is-enabled mysqld.service'
    }

    Package['mysql'] ->
    Exec['enable service'] ->
    Exec['start']
}
