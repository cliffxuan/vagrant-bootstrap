class python_dev {
    case $operatingsystem {
      'ubuntu': {
        require python_dev::ubuntu
        }
      'centos':  {
        require python_dev::centos
        }
    }
}

class python_dev::ubuntu {
    package {'python-dev':
        ensure => installed
    }
}

class python_dev::centos {
    package {'python-devel':
        ensure => installed
    }
}
