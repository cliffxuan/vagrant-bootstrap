class python27 {
    case $operatingsystem {
      'centos':  {
        require python27::centos
        }
      'archlinux': {
        require python27::archlinux
      }
    }
}

class python27::archlinux {
    package {python2:
        ensure => installed
    }
    file { '/usr/bin/python':
        ensure => link,
        target => '/usr/bin/python2.7'
    }
}

class python27::centos {
    include python_dev
    include python_deps

    exec {'download src':
        command => 'rm -rf Python-2.7.6*&& wget http://www.python.org/ftp/python/2.7.6/Python-2.7.6.tgz&& tar xf Python-2.7.6.tgz',
        cwd => '/tmp/',
        unless => 'which python2.7'
    }
    exec {'compile and install':
        command => 'sh configure --prefix=/usr/local&& make&& make install',
        cwd => '/tmp/Python-2.7.6',
        unless => 'which python2.7'
    }

    Class['python_dev'] ->
    Class['python_deps'] ->
    Exec['download src'] ->
    Exec['compile and install']
}
