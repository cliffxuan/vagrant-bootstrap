class basic_python {

    include git
    include python27
    include easy_install
    include libxml2_dev
    include libxslt_dev
    include python_dev

    #soft link pip-python is for centos only. puppet for centos
    #expects pip-python instead of pip
    exec { "install pip":
        command => "python2.7 -m easy_install pip; ln -s `which pip` /usr/bin/pip-python",
        timeout => 600,
        require => Class['easy_install'],
        unless  => 'python2.7 -c "import pip"'
    }

    package { [ "libssl-dev",
                "libffi-dev"]:
      ensure => latest
    }

    package { [ "virtualenv",
                "ipython",
                "ipdb",
                "flake8",
                "virtualenvwrapper" ]:
        ensure => latest,
        provider => pip,
        require => Exec["install pip"]
    }
}
