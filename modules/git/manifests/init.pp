class git {
    $pkg = $operatingsystem ? {
            Ubuntu  => "git-core",
            default => "git",
        }
    package {"$pkg": ensure => installed}
}
