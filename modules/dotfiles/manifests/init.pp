class dotfiles($checkout_dir, $user) {

    include git

    exec { "checkout dotfiles":
        cwd => "${checkout_dir}",
        command => "git clone git@github.com:cliffxuan/dotfiles.git",
        creates => "${checkout_dir}/dotfiles/.git",
        require => [ Class["git"], File[$checkout_dir] ],
        user => $user
    }

    exec { "kickoff":
        command => "${checkout_dir}/dotfiles/kickoff.sh",
        user => $user,
        environment => ["HOME=/home/$user"],
    }

    Exec["checkout dotfiles"] ->
    Exec["kickoff"]
}
