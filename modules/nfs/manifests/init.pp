class nfs::params {
    case $operatingsystem {
      'archlinux':  {
        $package = ['nfs-utils', 'libnfs']
        /*it's currently broken for arch, promised fix will be in 3.2 https://projects.puppetlabs.com/issues/18129*/
        /*$service = ['rpc-idmapd.service', 'rpc-mountd.service']*/
        $service = []
        file {'/usr/lib/systemd/system/rpc-mountd.service':
            ensure => link,
            target => '/etc/systemd/system/multi-user.target.wants/rpc-mountd.service'
            }
        file {'/usr/lib/systemd/system/rpc-idmapd.service':
            ensure => link,
            target => '/etc/systemd/system/multi-user.target.wants/rpc-idmapd.service'
            }
        }
      'ubuntu': {
        $package = 'nfs-kernel-server'
        $service = 'nfs-kernel-server'
        }
      'centos':  {
        $package = ['nfs-utils', 'nfs-utils-lib']
        $service = 'nfs'
        }
    }
}

class nfs inherits nfs::params {
    package { $package:
        ensure=> installed
    }
    service { $service:
        ensure => 'running'
    }

    Package[$package] -> Service[$service]
}
