class easy_install {
    case $operatingsystem {
      'ubuntu': {
        require easy_install::ubuntu
        }
      'centos':  {
        require easy_install::centos
        }
      'archlinux': {
        require easy_install::archlinux
      }
    }
}

class easy_install::ubuntu {
    package {'python-distribute':
        ensure => installed
    }
}

class easy_install::archlinux {
    require python27
    package {'python2-distribute':
        ensure => installed
    }
}

class easy_install::centos {
    require python27
    exec {
        'curl -O http://python-distribute.org/distribute_setup.py; python2.7 distribute_setup.py':
        cwd => '/tmp',
        unless => 'python2.7 -c "import setuptools"'
    }
}
