class repositories::remi {

    yumrepo { remi:
	mirrorlist  => 'http://rpms.famillecollet.com/enterprise/$releasever/remi/mirror',
        descr       => 'Les RPM de remi en test pour Enterprise Linux $releasever - $basearch',
        enabled     => 1,
        gpgcheck    => 1,
        gpgkey      => "file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi",
        includepkgs => 'php* mysql* python* compat-mysql51*',

    }

    file { "/etc/pki/rpm-gpg/RPM-GPG-KEY-remi":
        ensure      => present,
        owner       => root,
        group       => root,
        mode        => 0644,
        source      => "puppet:///modules/repositories/RPM-GPG-KEY-remi",
    }
}
