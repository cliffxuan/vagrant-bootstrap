class mysql_superuser {
    require mysql

    exec{ "create user user":
        command => "mysql -u root -e \"grant all privileges on *.* to 'user'@'%' identified by 'password'\" \
                &&  mysql -u root -e \"grant all privileges on *.* to 'user'@'localhost' identified by 'password'\"",
        unless  => "mysql -u user -ppassword"
    }
}
