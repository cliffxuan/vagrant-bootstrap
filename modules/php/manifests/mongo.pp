class php::mongo {
    include php::54

    case $operatingsystem {
      'centos':  {
        require php::mongo::centos
        }
    }
}

class php::mongo::centos {

    include repositories::remi

    package {[
        "php-pecl-mongo"]:
        ensure => installed,
        require => Yumrepo['remi']
    }
}
