class php::memcache {
  package { 'php-pecl-memcache':
    ensure => 'installed',
    notify => Service['httpd']
  }
}
