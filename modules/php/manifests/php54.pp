class php::php54 {
    case $operatingsystem {
      'ubuntu': {
        require php::php54::ubuntu
        }
      'centos':  {
        require php::php54::centos
        }
      'archlinux':  {
        require php::php54::ubuntu
        }
    }
}

class php::php54::ubuntu {

    package { [
        "php5-cli",
        "php5-mysql",
        "php5-curl"
        ]:
        ensure => installed,
    }
}

class php::php54::centos {

    include repositories::remi

    package {[
        "php",
        "php-mysql"]:
        ensure => installed,
        require => Yumrepo['remi']
    }
}
