class python_deps {
    case $operatingsystem {
      'centos':  {
        require python_deps::centos
        }
    }
}

class python_deps::centos {
    package {[
       'zlib-devel',
       'bzip2-devel',
       'openssl-devel',
       'ncurses-devel',
       'sqlite-devel',
       'readline-devel',
       'tk-devel']:
      ensure => installed
    }
}
