class nfs_config {
    require nfs

    file {'/etc/exports':
        ensure => file,
        content  => template('nfs_config/exports.erb'),
    }

    exec {'exportfs -a':
        require => File['/etc/exports'],
        unless  => 'exportfs -v | grep ${user}',
    }
}
