class libxslt_dev {
    case $operatingsystem {
      'ubuntu': {
        require libxslt_dev::ubuntu
        }
      'centos':  {
        require libxslt_dev::centos
        }
    }
}

class libxslt_dev::ubuntu {
    package {'libxslt-dev':
        ensure => installed
    }
}

class libxslt_dev::centos {
    package {'libxslt-devel':
        ensure => installed
    }
}
